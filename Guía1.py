# Nombre: Gabriela Antonia Sepúlveda Rojas
# RUT: 20.787.581-3
# Fecha de entrega: 17 de diciembre

# Crear una pentana principal que incluya el evento del cierre de aplicación
# al momento de presionar los botones de accion nativos para cerrar. Deberá contener
# dos cuadros de texto en el cual podrán ingresar un tetxo cualquiera para realizar la sumatoria.
# El boton "Aceptar" al ser presionado levantará la ventana de diálogo para mostrar los valores ingresados y el resultado
# de la sumatoria obtenida de la ventana principal

# Entradas: Para comenzar, se importa la librería GI (Gobject Instrospection) para liego seleccionar la versión específica a utilizar.

# Desarrollo: Comenzaremos creando la clase donde programaremos todos los elementos que estarán presentes, creando así la ventana principal, el llamado
# al programa, las entradas de texto y los botones, los cuales tendrán una funcion específica segun cuál sea.

# Salidas: Podremos ver que la ventana principal tendrá dos botones "Aceptar/Reset", mientras que la de diálogo tendrá dos botones diferentes "Aceptar/Cancelar"
# ,también se imprimirá en la pantalla los valores ingresados y el resultado de su suma. 

import gi 
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class VentanaPrincipal():

    def __init__(self):

        builder = Gtk.Builder()
        builder.add_from_file("guia1_interfaz.ui")

        
        Ventana = builder.get_object("Principal")
        Ventana.connect("destroy", Gtk.main_quit)
        Ventana.set_title("Guía")
        Ventana.maximize()

        #################################################################

        self.entry1 = builder.get_object("Entrada_txt1")
        self.entry1.connect("activate", self.sumar_texto)
        self.entry1.set_placeholder_text("Ingrese el texto o un número: ")

        self.entry2 = builder.get_object("Entradatxt2")
        self.entry2.connect("activate", self.sumar_texto)
        self.entry2.set_placeholder_text("Ingrese texto o un número: ")

        ##################################################################

        self.aceptar = builder.get_object("Botón_aceptar")
        self.aceptar.connect("clicked", self.dialogo)

        self.deshacer = builder.get_object("Botón_deshacer")
        self.deshacer.connect("clicked", self.limpiar)

        ###################################################################

        label_texto1 = builder.get_object("Label 1")
        label_texto1.set_label("Entrada: ")

        label_texto2 = builder.get_object("Label 2")
        label_texto2.set_label("Entrada: ")

        self.suma = builder.get_object("Label 3")
        self.suma.set_label("Suma: ")

        Ventana.show_all()


    def sumar_texto(self, enter=None):

        self.text1 = self.entry1.get_text()
        self.text2 = self.entry2.get_text()

        if self.text1.isnumeric():
            Valor1 = int(self.text1)
        else:
            Valor1 = len(self.text1)


        if self.text2.isnumeric():
            Valor2 = int(self.text2)
        else:
            Valor2 = len(self.text2)


        Suma = Valor1 + Valor2

        self.suma.set_label(f'suma: {str(Suma)}')


    def dialogo(self, btn=None):

        self.sumar_texto()
        self.ventana_dialogo = dialogo(self.text1, self.text2, self.suma)

        response = self.ventana_dialogo.ventana.run()

        if response == Gtk.ResponseType.OK:

            if self.text1.isnumeric():
                self.entry1.set_text("0")
            else:
                self.entry1.set_text("")


            if self.text2.isnumeric():
                self.entry2.set_text("0")
            else:
                self.entry2.set_text("")

        self.ventana_dialogo.ventana.destroy()


    def limpiar(self, btn=None):
        self.entry1.set_text("")
        self.entry2.set_text("")
        self.suma.set_label("Suma:")


class dialogo():

    def __init__(self, text1, text2, suma):

        self.builder = Gtk.Builder()
        self.builder.add_from_file("guia1_interfaz.ui")

        self.ventana = self.builder.get_object("Resultados")
 
        self.ventana.format_secondary_text("Se ha ingresado al sistema "f"{text1} y {text2}""teniendo como resultado: "f"{suma.get_text()}")

        self.ventana.show_all()

if __name__ == "__main__":
    VentanaPrincipal()
    Gtk.main()